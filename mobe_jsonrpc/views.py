import os
import json
import fnmatch
from bisect import bisect_left
from django.shortcuts import render
from jsonrpc import jsonrpc_method
from mobe.settings import DATA_DIR

import logging
log = logging.getLogger(__name__)

from uWave import *

KNEIGHBORS = 2
BORDER = 0.7

############################################################
def createFoldersForUser(username, modelname):
    userFolder = os.path.join(DATA_DIR, username)
    modelFolder = os.path.join(userFolder, modelname)
    
    trainDataFolder = os.path.join(modelFolder, "train")
    testDataFolder = os.path.join(modelFolder, "test")
    attackDataFolder = os.path.join(modelFolder, "attack")
    
    # Create train data folder if it does not exist
    if not os.path.exists(trainDataFolder):
        os.makedirs(trainDataFolder)
        
    # Create test data folder if it does not exist
    if not os.path.exists(testDataFolder):
        os.makedirs(testDataFolder)
        
    # Create attack data folder if it does not exist
    if not os.path.exists(attackDataFolder):
        os.makedirs(attackDataFolder)
        
def writeDataToFile(data, filepath):
    with open(filepath, 'w') as out_file:
        out_file.write(json.dumps(json.loads(data), sort_keys = True, indent = 4, separators = (',', ': ')))
        
def getQuantizedData(folder):
    dataList = []
    for file in os.listdir(folder):
        if fnmatch.fnmatch(file, '*.txt'):
            with open(os.path.join(folder, file), 'r') as trainFileName:
                trainFileString = trainFileName.read()
                trainFile = json.loads(trainFileString)
                trainFileData = trainFile["params"]["data"]
                
                trainAccelerationData = [x["acceleration"] for x in trainFileData]
                trainFrequency = trainFile["params"]["frequency"]
                
                dataList.append(quantizeData(trainAccelerationData, trainFrequency))
                
    return dataList    
############################################################

@jsonrpc_method('mobe.ping')
def say_hello(request):
    log.info("Ping method was called")
    return "OK " + request.jsonrpc_version

@jsonrpc_method('mobe.trainUser(username=String, modelname=String, frequency=Number, date=Number, data=Object, additional=Object) -> String')
def trainUser(request, username, modelname, frequency, date, data, additional = None):
    #jsonobj = json.loads(request.body)
    #print json.dumps(jsonobj, sort_keys = True, indent = 4, separators = (',', ': '))
    log.info("Got data for train from " + username)
    
    # Create username folder, modelname folder and train folder if they don`t exist
    createFoldersForUser(username, modelname)
    userFolder = os.path.join(DATA_DIR, username)
    modelFolder = os.path.join(userFolder, modelname)
    trainDataFolder = os.path.join(modelFolder, "train")
        
    # Create file for train
    writeDataToFile(request.body, os.path.join(trainDataFolder, str(date) + ".txt"))
    log.info("Wrote train record for user " + username + ", model \"" + modelname + "\" to file \"" + str(date) + ".txt\"")
    
    # Read all training data from folder and quantize it
    trainList = getQuantizedData(trainDataFolder)
    log.info("Got quantized data for user " + username + ", model \"" + modelname + "\"")
    
    # Run knn for build data
    if (len(trainList) >= KNEIGHBORS + 1):
        log.info("Started training user " + username + ", model \"" + modelname + "\"")     
        distancesList = knn(trainList, 2)[0]
        log.info("Finished training user " + username + ", model \"" + modelname + "\"")
        
        outputKNNFile = os.path.join(modelFolder, "knn.txt")
        log.info("Writing trained model for user " + username + ", model \"" + modelname + "\" to file " + outputKNNFile)
        with open(outputKNNFile, 'w') as out_file:
            out_file.write(json.dumps(distancesList, sort_keys = True, indent = 4, separators = (',', ': ')))    
    
    return "Gesture successfully added!"

@jsonrpc_method('mobe.authentificateUser(username=String, modelname=String, frequency=Number, date=Number, data=Object, additional=Object) -> String')
def authentificateUser(request, username, modelname, frequency, date, data, additional = None):
    #jsonobj = json.loads(request.body)
    #print json.dumps(jsonobj, sort_keys = True, indent = 4, separators = (',', ': '))
    log.info("Got data for authentification from " + username)
    
    # Create username folder, modelname folder and test folder if they dont exist
    createFoldersForUser(username, modelname)
    userFolder = os.path.join(DATA_DIR, username)
    modelFolder = os.path.join(userFolder, modelname)
    testDataFolder = os.path.join(modelFolder, "test")
    trainDataFolder = os.path.join(modelFolder, "train")
        
    # Create file for auth record
    writeDataToFile(request.body, os.path.join(testDataFolder, str(date) + ".txt"))
    log.info("Wrote auth record for user " + username + ", model \"" + modelname + "\" to file \"" + str(date) + ".txt\"")
    
    accelerationslist = [x["acceleration"] for x in data]
    accelerationslist = quantizeData(accelerationslist, frequency)
    
    # Read all training data for model and quantize it
    trainList = getQuantizedData(trainDataFolder)
    log.info("Got quantized data for user " + username + ", model \"" + modelname + "\"")
         
    # Read model from saved file
    kNNFile = os.path.join(modelFolder, "knn.txt")
    distancesList = []
    with open(kNNFile, 'r') as in_file:
        distancesList = json.loads(in_file.read())
    log.info("Read built model for user " + username + ", model \"" + modelname + "\" from file \"" + kNNFile + "\"")
             
    distance = getNearestNeighbors(trainList, accelerationslist, KNEIGHBORS)
    
    print "K distances:"
    for elem in distance:
        print elem[-1]
    print "Distances:"
    for elem in distancesList:
        print elem
    print ""
    
    distance = distance[-1][-1]
    authResult = 1.0 - bisect_left(distancesList, distance) / float(len(distancesList))
    
    return ("Authentification of \"" + username + "\"\nResult: " + str(authResult))

@jsonrpc_method('mobe.attackUser(username=String, usernameToAttack=String, modelname=String, frequency=Number, date=Number, data=Object, additional=Object) -> String')
def attackUser(request, username, usernameToAttack, modelname, frequency, date, data, additional = None):
    #jsonobj = json.loads(request.body)
    #print json.dumps(jsonobj, sort_keys = True, indent = 4, separators = (',', ': '))
    log.info("Got data for attack from " + username)
    
    # Create username folder, modelname folder and attack folder if they dont exist
    userFolder = os.path.join(DATA_DIR, username)
    attackUserFolder = os.path.join(DATA_DIR, usernameToAttack)
    
    modelFolder = os.path.join(userFolder, modelname)
    attackModelFolder = os.path.join(attackUserFolder, modelname)
    
    trainDataFolder = os.path.join(attackModelFolder, "train")
    attackDataFolder = os.path.join(attackModelFolder, "attack")
    createFoldersForUser(usernameToAttack, modelname)
        
    # Create file for attack record
    writeDataToFile(request.body, os.path.join(attackDataFolder, str(date) + ".txt"))
    log.info("Wrote attack record for user " + usernameToAttack + ", model \"" + modelname + "\" to file \"" + str(date) + ".txt\"")    
    
    accelerationslist = [x["acceleration"] for x in data]
    accelerationslist = quantizeData(accelerationslist, frequency)
    
    # Read all training data for model and quantize it
    trainList = getQuantizedData(trainDataFolder)
    log.info("Got quantized data for user " + username + ", model \"" + modelname + "\"")
          
    # Read model from saved file
    kNNFile = os.path.join(attackModelFolder, "knn.txt")
    distancesList = []
    with open(kNNFile, 'r') as in_file:
        distancesList = json.loads(in_file.read())
    log.info("Read built model for user " + usernameToAttack + ", model \"" + modelname + "\" from file \"" + kNNFile + "\"")
              
    distance = getNearestNeighbors(trainList, accelerationslist, KNEIGHBORS)
    
    print "K distances:"
    for elem in distance:
        print elem[-1]
    print "Distances:"
    for elem in distancesList:
        print elem
    print ""
    
    distance = distance[-1][-1]
    authResult = 1.0 - bisect_left(distancesList, distance) / float(len(distancesList))
    
    return ("Attack \"" + username + "\" on user \"" + usernameToAttack + "\"\nResult " + str(authResult))