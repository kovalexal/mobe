from django.conf.urls import patterns, url
from jsonrpc import jsonrpc_site

import mobe_jsonrpc.views

urlpatterns = patterns('',
    url(r'^$', jsonrpc_site.dispatch, name="jsonrpc_mountpoint"),
    #url(r'^browse/', 'jsonrpc.views.browse', name = 'jsonrpc_browser'), # for the graphical browser/web console only, omissible
    #(r'^(?P<method>[a-zA-Z0-9.]+)$', jsonrpc_site.dispatch) # for HTTP GET only, also omissible
)
