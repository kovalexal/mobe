// Stores process data function, which is different on every page
var processDataFunction = null;
// Stores accelerometer and gyroscope recorded data
var recordedData = null;
// Stores gesture start time
var gestureStartTime = null;
// Stores OS name
var OSName = null;
// Stores OS impact on sensors data
var OSImpact = {
	x     : 1.0,
	y     : 1.0,
	z     : 1.0,
	alpha : 1.0,
	beta  : 1.0,
	gamma : 1.0
};

// Function which processes data got from sensors and stores it in recordedData
function recordData(e) {
	var currentTime = Date.now();

	recordedData.push({
		time: currentTime,
		acceleration: [e.x * OSImpact.x, e.y * OSImpact.y, e.z * OSImpact.z],
		rotation: [e.alpha * OSImpact.alpha, e.beta * OSImpact.beta, e.gamma * OSImpact.gamma],
	});
};

// Function, which is invoked when the user presses "Start" button
function startRecordingData() {
	recordedData = [];

	if (window.DeviceOrientationEvent || window.DeviceMotionEvent) {
		$(this).text("Stop");
		$(this).unbind("click").click(finishRecordingData);

		gestureStartTime = Date.now();
		gyro.startTracking(recordData);
	} else {
		alert("Your device does not have accelerometer!");
	}
};

// Function, which is invoked when the user presses "Stop" button
function finishRecordingData() {
	gyro.stopTracking();

	$(this).text("Start");
	$(this).unbind("click").click(startRecordingData);

	var processData = confirm("Send data to server?");
	if (processData == true) {
		processDataFunction();
	}
};

// Returns operating system name (using user agent for that)
function getOperatingSystemName() {
	var userAgent = navigator.userAgent || navigator.vendor || window.opera;

	if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) ) {
		return 'iOS';
	} else if( userAgent.match( /Android/i ) ) {
		return 'Android';
	} else {
		return 'unknown';
	}
}

$("document").ready(function () {
	// Setting frequency
	frequency = Number(localStorage.getItem("frequency"));
	gyro.frequency = frequency === null ? 20 : frequency;

	// Quering OS and changing its impact on sensors data
	OSName = getOperatingSystemName();
	if (OSName == "Android") {
		OSImpact.y = -1.0;
		OSImpact.z = -1.0;
	}
});