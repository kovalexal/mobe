$("document").ready(function (){
	var usernameField = $("#username");
	var frequencyField = $("#frequency");

	// Load settings from browser localStorage
	$("#settingsForm").ready(function (){
		var username = localStorage.getItem("username");
		var frequency = localStorage.getItem("frequency");

		usernameField.val(username);
		frequencyField.val(frequency);
		frequencyField.prop("value", frequency);
		frequencyField.change();
	});

	// Saves settings to browser localStorage
	$("#settingsForm").submit(function (event) {
		var username = $("#username").val();
		var frequency = Number($("#frequency").val());

		localStorage.setItem("username", username);
		localStorage.setItem("frequency", frequency);

		// Set frequency for gyro library
		gyro.frequency = frequency;

		alert("Settings Saved!");

		event.preventDefault();
	});

	// Sign Out a user and clear all fields
	$("#signOutButton").click(function () {
		localStorage.clear();

		usernameField.val("");
		frequencyField.val("20");
		frequencyField.change();

		alert("Successful sign out!")
	});

	// Assign data processing function to training page
	$("#trainingPage").on("pageshow", function() {
		processDataFunction = sendTrainingData;
	});

	// Assign data processing function to auth page
	$("#authPage").on("pageshow", function() {
		processDataFunction = sendAuthentificationData;
	});

	// Assign data processing function to training page
	$("#attackPage").on("pageshow", function() {
		processDataFunction = sendAttackData;
	});

	// Save modelname in browsers localStorage
	$(".modelname").change(function () {
		// We have different modelname in multiple forms, so using this
		var modelname = $(this).val();
		localStorage.setItem("modelname", modelname);
		$(".modelname").val(modelname);

		alert("Model saved");
	});

	// Load model name from browsers localStorage
	$(".modelname").ready(function () {
		var modelname = localStorage.getItem("modelname");
		$(".modelname").val(modelname);
	});

	// Prevent processing modelnameToTrain form
	$(".modelnameForm").submit(function (event) {
		event.preventDefault();
	});

	// Save attack username in browsers localStorage
	$(".usernameToAttack").change(function () {
		var usernameToAttack = $(".usernameToAttack").val();
		localStorage.setItem("usernameToAttack", usernameToAttack);
		$(".usernameToAttack").val(usernameToAttack);

		alert("Attack username saved");
	});

	// Load attack username from browsers localStorage
	$(".usernameToAttack").ready(function () {
		var usernameToAttack = localStorage.getItem("usernameToAttack");
		$(".usernameToAttack").val(usernameToAttack);
	});

	// Prevent processing attack username form
	$(".attackUserForm").submit(function (event) {
		event.preventDefault();
	});

	// Sign click action to startRecordingData action
	$(".recordDataButton").click(startRecordingData);
});