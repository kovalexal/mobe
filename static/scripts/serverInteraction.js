// Processes data on training
function sendTrainingData() {
	//alert("Send Training Data");
	//alert(JSON.stringify(recordedData, undefined, 4));

	var sUsername = localStorage.getItem("username");
	var sModelname = localStorage.getItem("modelname");
	var nFrequency = Number(localStorage.getItem("frequency"));

	invokeJSONRPCMethod(serverAddress, "mobe.trainUser", { username: sUsername, modelname: sModelname, frequency: nFrequency, date: gestureStartTime, data: recordedData, additional: { OS: OSName } });
};

// Processes data on authentification
function sendAuthentificationData() {
	//alert("Send Authentification Data");
	//alert(JSON.stringify(recordedData, undefined, 4));

	var sUsername = localStorage.getItem("username");
	var sModelname = localStorage.getItem("modelname");
	var nFrequency = Number(localStorage.getItem("frequency"));

	invokeJSONRPCMethod(serverAddress, "mobe.authentificateUser", { username: sUsername, modelname: sModelname, frequency: nFrequency, date: gestureStartTime, data: recordedData, additional: { OS: OSName } });
};

// Processes data on attack
function sendAttackData() {
	//alert("Send Attack Data");
	//alert(JSON.stringify(recordedData, undefined, 4));

	var sUsername = localStorage.getItem("username");
	var sUsernameToAttack = localStorage.getItem("usernameToAttack");
	var sModelname = localStorage.getItem("modelname");
	var nFrequency = Number(localStorage.getItem("frequency"));

	invokeJSONRPCMethod(serverAddress, "mobe.attackUser", { username: sUsername, usernameToAttack: sUsernameToAttack, modelname: sModelname, frequency: nFrequency, date: gestureStartTime, data: recordedData, additional: { OS: OSName } });
};