// Returns unique number
var guid = (function() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
               .toString(16)
               .substring(1);
  }
  return function() {
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
           s4() + '-' + s4() + s4() + s4();
  };
})();

// Stores a unique id for every session (for jsonrpc id)
var uuid = guid();

// The default JSON-RPC success handler
function jsonRPCSuccess(oRepsonse) {
	alert(oRepsonse.result);
};

// The default JSON-RPC complete handler
function jsonRPCComplete() {
};

// The default JSON-RPC error handler
function jsonRPCError(xhr, textStatus, errorThrown) {
	if (xhr.status === 0) {
		alert('Cannot connect.\nVerify Network.');
	} else if (xhr.status == 404) {
		alert('Requested page not found. [404]');
	} else if (xhr.status == 500) {
		alert('Internal Server Error [500].');
	} else if (textStatus === 'parsererror') {
		alert('Requested JSON parse failed.');
	} else if (textStatus === 'timeout') {
		alert('Time out error.');
	} else if (textStatus === 'abort') {
		alert('Ajax request aborted.');
	} else {
		alert('Uncaught Move Unknown Ticket Error.\n' + jqXHR.responseText);
	}
};

// Invokes JSON-RPC on server
function invokeJSONRPCMethod(sRequestURL, sMethodName, oParams, fSuccess, fError, fComplete) {
	fSuccess = typeof fSuccess !== 'undefined' ? fSuccess : jsonRPCSuccess;
	fError = typeof fError !== 'undefined' ? fError : jsonRPCError;
	fComplete = typeof fComplete !== 'undefined' ? fComplete : jsonRPCComplete;

	var request = {};
	request.method = sMethodName;
	request.params = oParams;
	request.id = uuid;
	request.jsonrpc = "2.0";

	$.ajax({
		type: "POST",
		url: sRequestURL,
		dataType: "json",

		data: JSON.stringify(request),

		success: fSuccess,
		error: fError,
		complete: fComplete,
	});
};